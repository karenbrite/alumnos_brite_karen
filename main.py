import tkinter as tk

from centrar_ventana import centrar
from controlers.carreras_controler import carreras_controler
from controlers.cursos_controler import cursos_controler
from controlers.alumnos_controler import alumnos_controler

class mainView:
    def __init__(self, root):
        self.root = root
        self.root.title("Administración Instituto")
        
        self.menubar = tk.Menu(master=root)
        # Asigna menubar a root.
        root.config(menu=self.menubar)
        # Establece el tamaño de la ventana
        root.geometry(centrar(ancho=640, alto=410, app=self.root))
        # Fija el redimensionamiento de la ventana
        root.resizable(False, False)

        
          

        # Crea systemmenu.
        self.systemmenu =  tk.Menu(master=self.menubar, tearoff=False)
        # Añade las opciones a systemmenu.
        self.systemmenu.add_command(label='Editar Perfil')
        self.systemmenu.add_command(label='Preferencias')
        self.systemmenu.add_command(label='Configuración')
        # Pone una línea separadora horizontal.
        self.systemmenu.add_separator()
        self.systemmenu.add_cascade(label='Salir', command=self.root.quit)
        self.systemmenu.configure(bg="light Blue", fg="blue")
        # Crea filemenu.
        self.filemenu = tk.Menu(self.menubar, tearoff=False)
        # Añade las etiquetas de los ítems.
        self.filemenu.add_command(label="Carreras", command=lambda: self.toggle_menu(1))
        self.filemenu.add_command(label="Cursos", command=lambda: self.toggle_menu(2))
        self.filemenu.add_command(label="Alumnos", command=lambda: self.toggle_menu(3))
        self.filemenu.configure(bg="orange", fg="green")
        # Crea reportmenu.
        self.reportmenu = tk.Menu(master=self.menubar, tearoff=False)
        # Añade los ítems reportmenu.
        self.reportmenu.add_command(label="Carreras")
        self.reportmenu.add_command(label="Cursos")
        self.reportmenu.add_command(label="Alumnos")
        self.reportmenu.configure(bg="purple", fg="turquoise")
        # Crea helpmenu.
        self.helpmenu = tk.Menu(master=self.menubar, tearoff=False)
        # Añade los ítems a helpmenu.
        self.helpmenu.add_command(label="Ayuda")
        self.helpmenu.add_separator()
        self.helpmenu.add_command(label="Acerca de...")
        self.helpmenu.configure(bg="violet", fg="blue")
        # Añade al menu bar los submenu mediante la propiedad menu.
        self.menubar.add_cascade(label="Sistema", menu=self.systemmenu)
        self.menubar.add_cascade(label="Archivos", menu=self.filemenu)
        self.menubar.add_cascade(label="Informes", menu=self.reportmenu)
        self.menubar.add_cascade(label="Ayuda", menu=self.helpmenu)

    def toggle_menu(self, opcion: int) -> None:
        self.menubar.entryconfig(1, state="disabled")

        if opcion == 1:
            carreras_controler(self.root)
        elif opcion == 2:
            cursos_controler(self.root)
        elif opcion == 3:
            alumnos_controler(self.root)

        self.menubar.entryconfig(1, state="normal")

def main() -> None:
    '''main Función principal.
    '''
    # Crea un un objeto ventana principal con nombre root.
    root = tk.Tk()
    # Crea la instancia de la clase mainView pero no se asigna a una
    # variable por lo que no se podrá hacer referencia más adelante.
    ventana = mainView(root)
    #Cambiar el color de la ventana a rosa.
    root.config(bg="pink")

    # Levanta el bucle principal de eventos de la instacia root.
    root.mainloop()

if __name__ == "__main__":
    # Llamada a la función main.
    main()
