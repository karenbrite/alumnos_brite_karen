'''
alumnos_controler.py: Contiene el controlador de Alumnos.
'''
from views.alumnos_view import alumnos_view, modalWindow
from models.alumnos_model import alumnos_model

class alumnos_controler():
    '''alumnos_controller se encarga de manejar la vista y el modelo.
    '''
    def __init__(self, root: object) -> None:
        self.root = root
        # Instancia el modelo.
        self.model = alumnos_model()
        # Crea  un istancia la vista.
        self.view = alumnos_view(self.root)
        # Añade la función addToTreeview al botón de agregar.
        self.view.buttonAdd["command"] = self.addToTreeview
        # Añade la función removeFromTreeview al botón de eliminar.
        self.view.buttonUpdate["command"] = self.updateFromTreeview
        # Añade la función updateFromTreeview al botón de actualizar.
        self.view.buttonRemove["command"] = self.removeFromTreeview
        # Añade la función loadTreeviewToEntry al evento de selección de fila.
        self.view.buttonExit["command"] = self.__del__

        # Añade la función loadTreeviewToEntry al evento de selección de fila.
        #self.view.treeview.bind('<<TreeviewSelect>>', self.loadTreeviewToEntry)
        # Carga los datos de la base de datos al treeview.
        self.loadToTreeview()

        pass

    def loadToTreeview(self):
        '''loadToTreeview Carga los datos de la base de datos al treeview.
        '''
        data = self.model.getAllData()
        self.view.setTreeview(data)

    def addToTreeview(self):
        '''addToTreeview Agrega un registro a la base de datos y al treeview.'''
        # Crea una instancia de la ventana modal
        self.modal = modalWindow(self.root, 'Altas de alumnos')

        #Solo si hay un registro seleccionado el el treeview.
        if self.modal.buttonClicked:
            if self.modal.textvarApellido.get() != '' and \
                self.modal.textvarNombre.get() != '' and \
                self.modal.textvarInscripcion.get() != None and \
                self.modal.intvarCursos_id.get() != 0 :

                self.addToDB()
                self.loadToTreeview()
                self.clearForm()
            else:
                self.view.showMessageBox(message='Debe llenar todos los campos.', title='Error', type='error')

    def removeFromTreeview(self):
        '''removeFromTreeview Elimina un registro de la base de datos y del treeview.'''
        #Solo si hay un registro seleccionado el el treeview.
        if self.view.treeview.selection():
            # Crea una instancia de la ventana modal
            self.modal = modalWindow(self.root, 'Bajas de alumnos', self.loadTreeviewToEntry())

            if self.modal.buttonClicked:
                    self.removeFromDB()
                    self.loadToTreeview()
                    self.clearForm()

    def updateFromTreeview(self):
        '''updateFromTreeview Actualiza un registro de la base de datos y del treeview.'''
        #Solo si hay un registro seleccionado el el treeview.
        if self.view.treeview.selection():
            # Crea una instancia de la ventana modal
            self.modal = modalWindow(self.root, 'Modificación de alumnos', self.loadTreeviewToEntry())

            if self.modal.buttonClicked:
                    self.updateDB()
                    self.loadToTreeview()
                    self.clearForm()

    def loadTreeviewToEntry(self, event=None):
        #Solo si hay un registro seleccionado el el treeview.
        if self.view.treeview.selection():
            self.id = self.view.getCursorId()
            self.Apellido = self.view.getCursorApellido()
            self.Nombre = self.view.getCursorNombre()
            self.Inscripcion = self.view.getCursorInscripcion()
            self.Cursos_id = self.view.getCursorCursos_id()

        return (self.id,
                self.Apellido,
                self.Nombre,
                self.Inscripcion,
                self.Cursos_id)

    def addToDB(self):
        self.Apellido = self.modal.getApellido()
        self.Nombre = self.modal.getNombre()
        self.Inscripcion = self.modal.getInscripcion()
        self.Cursos_id = self.modal.getCursos_id()
        self.model.create(self.Apellido, self.Nombre, self.Inscripcion, self.Cursos_id)

    def updateDB(self):
        self.Id = self.modal.getId()
        self.Apellido = self.modal.getApellido()
        self.Nombre = self.modal.getApellido()
        self.Inscripcion = self.modal.getInscripcion()
        self.Cursos_id = self.modal.getCursos_id()
        self.model.update(self.Id, self.Apellido, self.Nombre, self.Inscripcion, self.Cursos_id)

    def removeFromDB(self):
        self.Id = self.view.getCursorId()
        self.model.delete(self.Id)

    def clearForm(self):
        self.modal.setId(0)
        self.modal.setApellido('')
        self.modal.setNombre('')
        self.modal.setInscripcion('')
        self.modal.setCursos_id(0)

        #Deselecciona fila de treeview.
        self.view.treeview.selection_remove(self.view.treeview.selection())

        return

    def __del__(self) -> None:
        #self.model.__del__
        #self.view.__del__

        self.view.frame2.destroy()
        self.view.frame3.destroy()
        self.view.frame4.destroy()

        #del self
